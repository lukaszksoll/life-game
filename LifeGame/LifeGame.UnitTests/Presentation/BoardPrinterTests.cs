﻿using FluentAssertions;
using LifeGame.Models;
using LifeGame.Presentation;
using NSubstitute;
using System;
using Xunit;

namespace LifeGame.UnitTests.Presentation
{
    public class BoardPrinterTests
    {
        [Fact]
        public void Print_WhenPrintBoard_ShouldClearScreen()
        {
            // Arrange
            const int givenOffset = 10;
            var mockConsoleManager = Substitute.For<IConsoleManager>();
            var stubBoard = Substitute.For<IBoard>();
            var sut = new BoardPrinter(mockConsoleManager);

            // Act
            sut.Print(stubBoard, givenOffset);

            // Assert
            mockConsoleManager.Received().Clear();
        }

        [Fact]
        public void Print_WhenInvalidOffset_ShouldThrowArgumentException()
        {
            // Arrange
            const int givenOffset = -1;
            var mockConsoleManager = Substitute.For<IConsoleManager>();
            var stubBoard = Substitute.For<IBoard>();
            var sut = new BoardPrinter(mockConsoleManager);

            // Act
            Action action = () => sut.Print(stubBoard, givenOffset);

            // Assert
            action.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void Print_WhenValidOffset_ShouldNotThrowArgumentException()
        {
            // Arrange
            const int givenOffset = 1;
            var mockConsoleManager = Substitute.For<IConsoleManager>();
            var stubBoard = Substitute.For<IBoard>();
            var sut = new BoardPrinter(mockConsoleManager);

            // Act
            Action action = () => sut.Print(stubBoard, givenOffset);

            // Assert
            action.Should().NotThrow<ArgumentException>();
        }

        [Theory]
        [InlineData(0, 0, 10, 10, 1, 13)]
        [InlineData(1, 1, 5, 5, 1, 7)]
        public void Print_WhenPrintBoard_CallsNewLineAsExpected(int minX, int minY, int maxX, int maxY, int offset, int expectedCalls)
        {
            // Arrange
            var mockConsoleManager = Substitute.For<IConsoleManager>();
            var stubBoard = Substitute.For<IBoard>();
            stubBoard.GetMaxX().Returns(maxX);
            stubBoard.GetMaxY().Returns(maxY);
            stubBoard.GetMinX().Returns(minX);
            stubBoard.GetMinY().Returns(minY);
            var sut = new BoardPrinter(mockConsoleManager);

            // Act
            sut.Print(stubBoard, offset);

            // Assert
            mockConsoleManager.Received(expectedCalls).WriteLine();
        }

        [Theory]
        [InlineData(0, 0, 10, 10, 1, 169)]
        [InlineData(1, 1, 5, 5, 1, 49)]
        public void Print_WhenPrintBoard_CallsWriteAsExpected(int minX, int minY, int maxX, int maxY, int offset, int expectedCalls)
        {
            // Arrange
            var mockConsoleManager = Substitute.For<IConsoleManager>();
            var stubBoard = Substitute.For<IBoard>();
            stubBoard.GetMaxX().Returns(maxX);
            stubBoard.GetMaxY().Returns(maxY);
            stubBoard.GetMinX().Returns(minX);
            stubBoard.GetMinY().Returns(minY);
            var sut = new BoardPrinter(mockConsoleManager);

            // Act
            sut.Print(stubBoard, offset);

            // Assert
            mockConsoleManager.Received(expectedCalls).Write(Arg.Any<char>());
        }
    }
}
