﻿using FluentAssertions;
using LifeGame.Presentation;
using System;
using Xunit;

namespace LifeGame.UnitTests.Presentation
{
    public class ThreadManagerTests
    {
        [Fact]
        public void Sleep_WhenInvalidTime_ShouldThrowArgumentException()
        {
            // Arrange
            const int time = -1;
            var sut = new ThreadManager();

            // Act
            Action action = () => sut.Sleep(time);

            // Assert
            action.Should().Throw<ArgumentException>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(100)]
        public void Sleep_WhenValidTime_ShouldNotThrowArgumentException(int time)
        {
            // Arrange
            var sut = new ThreadManager();

            // Act
            Action action = () => sut.Sleep(time);

            // Assert
            action.Should().NotThrow<ArgumentException>();
        }
    }
}
