﻿using FluentAssertions;
using LifeGame.Models;
using Xunit;

namespace LifeGame.UnitTests.Models
{
    public class CellTests
    {
        [Fact]
        public void Cell_CallConstructor_ShouldInitializeCoordinates()
        {
            // Arrange
            int x = 10;
            int y = 10;
            ICell sut;

            // Act
            sut = new Cell(x, y);

            // Assert
            sut.X.Should().Be(x);
            sut.Y.Should().Be(y);
        }

        [Fact]
        public void Equals_CheckEquality_ShouldBeEqual()
        {
            // Arrange
            var first = new Cell(10, 10);
            var second = new Cell(10, 10);

            // Act
            var result = first.Equals(second);

            // Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void Equals_CheckEquality_ShouldNotBeEqual()
        {
            // Arrange
            var first = new Cell(10, 10);
            var second = new Cell(11, 11);

            // Act
            var result = first.Equals(second);

            // Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void GetHashCode_TwoObjectHashCodeComparison_ShouldBeEqual()
        {
            // Arrange
            var first = new Cell(10, 10);
            var second = new Cell(10, 10);

            // Act
            var firstResult = first.GetHashCode();
            var secondResult = second.GetHashCode();

            // Assert
            firstResult.Should().Be(secondResult);
        }
    }
}
