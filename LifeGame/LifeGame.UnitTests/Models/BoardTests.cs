﻿using FluentAssertions;
using LifeGame.Factories;
using LifeGame.Logic;
using LifeGame.Models;
using NSubstitute;
using Xunit;

namespace LifeGame.UnitTests.Models
{
    public class BoardTests
    {
        private static readonly ICell StubFirstCell = Substitute.For<ICell>();
        private static readonly ICell StubSecondCell = Substitute.For<ICell>();
        private static readonly ICell StubThirdCell = Substitute.For<ICell>();

        static BoardTests()
        {
            StubFirstCell.X.Returns(10);
            StubFirstCell.Y.Returns(10);

            StubSecondCell.X.Returns(0);
            StubSecondCell.Y.Returns(50);

            StubThirdCell.X.Returns(11);
            StubThirdCell.Y.Returns(11);
        }

        [Fact]
        public void Board_CallConstructor_ShouldBoardBeEmpty()
        {
            // Arrange
            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            IBoard sut;

            // Act
            sut = new Board(stubAlgorithm, stubFactory);

            // Assert
            sut.CurrentGeneration.Should().BeEmpty();
        }

        [Fact]
        public void GetMinX_WhenExampleDataGiven_ThenReturnsAsExpected()
        {
            // Arrange
            const int expectedResult = 0;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            IBoard sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);
            sut.CurrentGeneration.Add(StubSecondCell);

            // Act
            var result = sut.GetMinX();

            // Assert
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void GetMinY_WhenExampleDataGiven_ThenReturnsAsExpected()
        {
            // Arrange
            const int expectedResult = 10;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            IBoard sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);
            sut.CurrentGeneration.Add(StubSecondCell);

            // Act
            var result = sut.GetMinY();

            // Assert
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void GetMaxX_WhenExampleDataGiven_ThenReturnsAsExpected()
        {
            // Arrange
            const int expectedResult = 10;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);
            sut.CurrentGeneration.Add(StubSecondCell);

            // Act
            var result = sut.GetMaxX();

            // Assert
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void GetMaxY_WhenExampleDataGiven_ThenReturnsAsExpected()
        {
            // Arrange
            const int expectedResult = 50;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);
            sut.CurrentGeneration.Add(StubSecondCell);

            // Act
            var result = sut.GetMaxY();

            // Assert
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void AddCell_WhenNotExists_ShouldAddCellToBoard()
        {
            // Arrange
            const int x = 10;
            const int y = 10;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);

            // Act
            sut.AddCell(x, y);

            // Assert
            sut.CurrentGeneration.Should().NotBeEmpty();
        }

        [Fact]
        public void AddCell_WhenNotExists_ShouldNotAddDuplicate()
        {
            // Arrange
            const int x = 10;
            const int y = 10;
            const int expectedCount = 1;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);

            // Act
            sut.AddCell(x, y);
            sut.AddCell(x, y);

            // Assert
            sut.CurrentGeneration.Should().HaveCount(expectedCount);
        }

        [Fact]
        public void CellExists_WhenNotExists_ShouldNotFound()
        {
            // Arrange
            const int x = 10;
            const int y = 11;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);

            // Act
            var result = sut.CellExists(x, y);

            // Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void CellExists_WhenExists_ShouldBeFound()
        {
            // Arrange
            const int x = 10;
            const int y = 10;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);

            // Act
            var result = sut.CellExists(x, y);

            // Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void GetCellOrDefault_WhenExists_ShouldReturnsCell()
        {
            // Arrange
            const int x = 10;
            const int y = 10;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);

            // Act
            var result = sut.GetCellOrDefault(x, y);

            // Assert
            result.Should().Be(StubFirstCell);
        }

        [Fact]
        public void GetCellOrDefault_WhenNotExists_ShouldReturnsNull()
        {
            // Arrange
            const int x = 11;
            const int y = 11;

            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);

            // Act
            var result = sut.GetCellOrDefault(x, y);

            // Assert
            result.Should().BeNull();
        }

        [Theory]
        [InlineData(10, 10, 1)]
        [InlineData(0, 0, 0)]
        public void CountAliveNeighbours_WhenPointsAreGiven_ShouldReturnsAsExpected(int x, int y, int expectedNeighbours)
        {
            // Arrange
            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);
            sut.CurrentGeneration.Add(StubFirstCell);
            sut.CurrentGeneration.Add(StubSecondCell);
            sut.CurrentGeneration.Add(StubThirdCell);

            // Act
            var result = sut.CountAliveNeighbours(x, y);

            // Assert
            result.Should().Be(expectedNeighbours);
        }

        [Fact]
        public void Process_WhenCall_ShouldCallAlgorithm()
        {
            // Arrange
            var stubAlgorithm = Substitute.For<ILifeGameAlgorithm>();
            var stubFactory = Substitute.For<ICellFactory>();
            var sut = new Board(stubAlgorithm, stubFactory);

            // Act
            sut.Process();

            // Assert
            stubAlgorithm.Received().DetermineNextGeneration(Arg.Any<IBoard>());
        }
    }
}
