﻿using FluentAssertions;
using LifeGame.Logic;
using LifeGame.Models;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace LifeGame.UnitTests.Models
{
    public class GameTests
    {
        [Fact]
        public void Run_WhenBoardIsEmpty_ShouldThrowArgumentNullException()
        {
            // Arrange
            var stubBoard = Substitute.For<IBoard>();
            var stubSimulator = Substitute.For<ILifeGameSimulator>();
            var sut = new Game(stubBoard, stubSimulator);

            // Act
            Action action = () => sut.Run(null);

            // Assert
            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void Run_WhenInitTupleListIsEmpty_ShouldThrowArgumentException()
        {
            // Arrange
            var stubBoard = Substitute.For<IBoard>();
            var stubSimulator = Substitute.For<ILifeGameSimulator>();
            var inputList = new List<(int, int)>();
            var sut = new Game(stubBoard, stubSimulator);

            // Act
            Action action = () => sut.Run(inputList);

            // Assert
            action.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void Run_WhenValidInputList_ShouldCallAddCell()
        {
            // Arrange
            var mockBoard = Substitute.For<IBoard>();
            var stubSimulator = Substitute.For<ILifeGameSimulator>();
            var inputList = new List<(int, int)>
            {
                (0, 0),
                (1, 0),
            };
            var sut = new Game(mockBoard, stubSimulator);

            // Act
            sut.Run(inputList);

            // Assert
            mockBoard.Received(inputList.Count).AddCell(Arg.Any<int>(), Arg.Any<int>());
        }

        [Fact]
        public void Run_WhenValidData_ShouldRunSimulator()
        {
            // Arrange
            var stubBoard = Substitute.For<IBoard>();
            var mockSimulator = Substitute.For<ILifeGameSimulator>();
            var inputList = new List<(int, int)>
            {
                (0, 0),
                (1, 0),
            };
            var sut = new Game(stubBoard, mockSimulator);

            // Act
            sut.Run(inputList);

            // Assert
            mockSimulator.Received().Simulate(stubBoard);
        }
    }
}
