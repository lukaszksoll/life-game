﻿using FluentAssertions;
using LifeGame.Factories;
using LifeGame.Models;
using NSubstitute;
using System;
using Xunit;

namespace LifeGame.UnitTests.Factories
{
    public class CellFactoryTests
    {
        [Fact]
        public void GetCell_WhenCoordinatesAreGiven_ShouldCallDelegate()
        {
            // Arrange
            const int x = 1;
            const int y = 1;
            var mockFunc = Substitute.For<Func<int, int, ICell>>();
            var sut = new CellFactory(mockFunc);

            // Act
            sut.GetCell(x, y);

            // Assert
            mockFunc.Received(1)(Arg.Any<int>(), Arg.Any<int>());
        }

        [Fact]
        public void GetCell_WhenCoordinatesAreGiven_ShouldReturnNewInstanceCell()
        {
            // Arrange
            const int x = 1;
            const int y = 1;
            var mockCell = Substitute.For<ICell>();
            var stubFunc = Substitute.For<Func<int, int, ICell>>();
            stubFunc(Arg.Any<int>(), Arg.Any<int>()).Returns(mockCell);
            var sut = new CellFactory(stubFunc);

            // Act
            var result = sut.GetCell(x, y);

            // Assert
            result.Should().NotBeNull();
            result.Should().Be(mockCell);
        }

        [Fact]
        public void GetCell_WhenCoordinatesAreGiven_ShouldReturnNewInstanceWithAppropriateProperties()
        {
            // Arrange
            const int x = 1;
            const int y = 2;
            var mockCell = Substitute.For<ICell>();
            mockCell.X.Returns(x);
            mockCell.Y.Returns(y);
            var stubFunc = Substitute.For<Func<int, int, ICell>>();
            stubFunc(Arg.Any<int>(), Arg.Any<int>()).Returns(mockCell);
            var sut = new CellFactory(stubFunc);

            // Act
            var result = sut.GetCell(x, y);

            // Assert
            result.X.Should().Be(x);
            result.Y.Should().Be(y);
        }
    }
}
