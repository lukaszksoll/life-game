﻿using LifeGame.Factories;
using LifeGame.Logic;
using LifeGame.Models;
using NSubstitute;
using Xunit;

namespace LifeGame.UnitTests.Logic
{
    public class LifeGameAlgorithmTests
    {
        [Fact]
        public void DetermineNextGeneration_WhenCellExistsAndHasTwoNeighbours_CallGetCell()
        {
            // Arrange
            var stubFactory = Substitute.For<ICellFactory>();
            var mockBoard = Substitute.For<IBoard>();
            mockBoard.CellExists(Arg.Any<int>(), Arg.Any<int>()).Returns(true);
            mockBoard.CountAliveNeighbours(Arg.Any<int>(), Arg.Any<int>()).Returns(2);
            var sut = new LifeGameAlgorithm(stubFactory);

            // Act
            sut.DetermineNextGeneration(mockBoard);

            // Assert
            mockBoard.Received().GetCellOrDefault(Arg.Any<int>(), Arg.Any<int>());
        }

        [Fact]
        public void DetermineNextGeneration_WhenCellExistsAndHasTwoNeighbours_CallFactory()
        {
            // Arrange
            var mockFactory = Substitute.For<ICellFactory>();
            var stubBoard = Substitute.For<IBoard>();
            stubBoard.CellExists(Arg.Any<int>(), Arg.Any<int>()).Returns(false);
            stubBoard.CountAliveNeighbours(Arg.Any<int>(), Arg.Any<int>()).Returns(3);
            var sut = new LifeGameAlgorithm(mockFactory);

            // Act
            sut.DetermineNextGeneration(stubBoard);

            // Assert
            mockFactory.Received().GetCell(Arg.Any<int>(), Arg.Any<int>());
        }
    }
}
