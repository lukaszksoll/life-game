﻿using System;
using System.Threading;

namespace LifeGame.Presentation
{
    public class ThreadManager : IThreadManager
    {
        public void Sleep(int time)
        {
            if (time < 0)
                throw new ArgumentException(nameof(time));

            Thread.Sleep(time);
        }
    }

    public interface IThreadManager
    {
        void Sleep(int time);
    }
}
