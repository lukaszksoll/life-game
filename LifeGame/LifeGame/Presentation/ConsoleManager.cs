﻿using System;

namespace LifeGame.Presentation
{
    internal class ConsoleManager : IConsoleManager
    {
        public void Write(char @char)
        {
            Console.Write(@char);
        }

        public void WriteLine()
        {
            Console.WriteLine();
        }

        public void Clear()
        {
            Console.Clear();
        }
    }

    public interface IConsoleManager
    {
        void Write(char @char);
        void WriteLine();
        void Clear();
    }
}
