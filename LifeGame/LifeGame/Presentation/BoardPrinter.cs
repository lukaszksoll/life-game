﻿using LifeGame.Models;
using System;

namespace LifeGame.Presentation
{
    public class BoardPrinter : IBoardPrinter
    {
        private const char EMPTY_CELL = '.';
        private const char ALIVE_CELL = '#';

        private readonly IConsoleManager consoleManager;

        public BoardPrinter(IConsoleManager consoleManager)
        {
            this.consoleManager = consoleManager;
        }

        public void Print(IBoard board, int offset)
        {
            if (offset < 0)
                throw new ArgumentException(nameof(offset));

            consoleManager.Clear();

            int minX = board.GetMinX();
            int minY = board.GetMinY();
            int maxX = board.GetMaxX();
            int maxY = board.GetMaxY();

            for (int j = minY - offset; j <= maxY + offset; ++j)
            {
                for (int i = minX - offset; i <= maxX + offset; ++i)
                {
                    consoleManager.Write(board.GetCellOrDefault(i, j) != null ? ALIVE_CELL : EMPTY_CELL);
                }

                consoleManager.WriteLine();
            }
        }
    }

    public interface IBoardPrinter
    {
        void Print(IBoard board, int offset);
    }
}
