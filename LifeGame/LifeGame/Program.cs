﻿using Autofac;
using LifeGame.Models;
using System.Collections.Generic;

namespace LifeGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<Bootstrapper>();

            var container = builder.Build();

            var game = container.Resolve<IGame>();

            var initCells = new List<(int, int)>
            {
                (1, 0),
                (2, 1),
                (0, 2),
                (1, 2),
                (2, 2),
            };

            game.Run(initCells);
        }
    }
}
