﻿using Autofac;
using LifeGame.Logic;
using LifeGame.Models;
using System;
using System.Reflection;
using Module = Autofac.Module;

namespace LifeGame
{
    internal class Bootstrapper : Module
    {
        private ContainerBuilder builder;

        protected override void Load(ContainerBuilder builder)
        {
            this.builder = builder;

            RegisterFactories();
            RegisterManagers();
            RegisterPrinters();
            RegisterModel();
            RegisterDelegates();
            RegisterAlgorithms();
        }

        private void RegisterFactories()
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Factory"))
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        private void RegisterManagers()
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Manager"))
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        private void RegisterPrinters()
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Printer"))
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        private void RegisterModel()
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AssignableTo<IBoard>()
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AssignableTo<ICell>()
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AssignableTo<IGame>()
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AssignableTo<ILifeGameSimulator>()
                .AsImplementedInterfaces();
        }

        private void RegisterDelegates()
        {
            builder.Register<Func<int, int, ICell>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();
                return (x, y) => new Cell(x, y);
            });
        }

        private void RegisterAlgorithms()
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Algorithm"))
                .AsImplementedInterfaces();
        }
    }
}
