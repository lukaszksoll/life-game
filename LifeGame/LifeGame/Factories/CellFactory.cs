﻿using LifeGame.Models;
using System;

namespace LifeGame.Factories
{
    public class CellFactory : ICellFactory
    {
        private readonly Func<int, int, ICell> cellResolver;

        public CellFactory(Func<int, int, ICell> cellResolver)
        {
            this.cellResolver = cellResolver;
        }

        public ICell GetCell(int x, int y)
        {
            return cellResolver(x, y);
        }
    }

    public interface ICellFactory
    {
        ICell GetCell(int x, int y);
    }
}
