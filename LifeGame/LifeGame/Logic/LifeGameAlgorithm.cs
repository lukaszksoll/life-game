﻿using LifeGame.Factories;
using LifeGame.Models;
using System.Collections.Generic;

namespace LifeGame.Logic
{
    public class LifeGameAlgorithm : ILifeGameAlgorithm
    {
        private readonly ICellFactory cellFactory;

        public LifeGameAlgorithm(ICellFactory cellFactory)
        {
            this.cellFactory = cellFactory;
        }

        public IEnumerable<ICell> DetermineNextGeneration(IBoard board)
        {
            var nextGenerationBoard = new HashSet<ICell>();

            int minX = board.GetMinX();
            int minY = board.GetMinY();
            int maxX = board.GetMaxX();
            int maxY = board.GetMaxY();

            for (int j = minY - 1; j <= maxY + 1; ++j)
            {
                for (int i = minX - 1; i <= maxX + 1; ++i)
                {
                    if (IsCellAliveAndHasTwoOrThreeNeighbours(i, j, board))
                    {
                        nextGenerationBoard.Add(board.GetCellOrDefault(i, j));
                    }
                    else if (CellNotExistsAndHasThreeNeighbours(i, j, board))
                    {
                        nextGenerationBoard.Add(cellFactory.GetCell(i, j));
                    }
                }
            }

            return nextGenerationBoard;
        }

        private bool IsCellAliveAndHasTwoOrThreeNeighbours(int x, int y, IBoard board)
        {
            var neighbours = board.CountAliveNeighbours(x, y);
            return board.CellExists(x, y) && (neighbours == 2 || neighbours == 3);
        }

        private bool CellNotExistsAndHasThreeNeighbours(int x, int y, IBoard board)
        {
            return !board.CellExists(x, y) && board.CountAliveNeighbours(x, y) == 3;
        }
    }

    public interface ILifeGameAlgorithm
    {
        IEnumerable<ICell> DetermineNextGeneration(IBoard board);
    }
}
