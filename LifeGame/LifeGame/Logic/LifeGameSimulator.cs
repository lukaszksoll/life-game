﻿using LifeGame.Models;
using LifeGame.Presentation;

namespace LifeGame.Logic
{
    public class LifeGameSimulator : ILifeGameSimulator
    {
        private const int PRINT_OFFSET = 10;
        private const int SLEEP_TIME = 500;
        private readonly IBoardPrinter printer;
        private readonly IThreadManager threadManager;

        public LifeGameSimulator(IBoardPrinter printer, IThreadManager threadManager)
        {
            this.printer = printer;
            this.threadManager = threadManager;
        }

        public void Simulate(IBoard board)
        {
            while (true)
            {
                printer.Print(board, PRINT_OFFSET);
                board.Process();
                threadManager.Sleep(SLEEP_TIME);
            }
        }
    }

    public interface ILifeGameSimulator
    {
        void Simulate(IBoard board);
    }
}
