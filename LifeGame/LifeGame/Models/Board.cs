﻿using LifeGame.Factories;
using LifeGame.Logic;
using System.Collections.Generic;
using System.Linq;

namespace LifeGame.Models
{
    public class Board : IBoard
    {
        private readonly ILifeGameAlgorithm algorithm;
        private readonly ICellFactory cellFactory;

        public Board(ILifeGameAlgorithm algorithm, ICellFactory cellFactory)
        {
            this.algorithm = algorithm;
            this.cellFactory = cellFactory;
        }

        public HashSet<ICell> CurrentGeneration { get; } = new HashSet<ICell>();

        public int GetMinX() => CurrentGeneration.Min(c => c.X);

        public int GetMinY() => CurrentGeneration.Min(c => c.Y);

        public int GetMaxX() => CurrentGeneration.Max(c => c.X);

        public int GetMaxY() => CurrentGeneration.Max(c => c.Y);

        public void AddCell(int x, int y)
        {
            if (!CellExists(x, y))
            {
                CurrentGeneration.Add(cellFactory.GetCell(x, y));
            }
        }

        public bool CellExists(int x, int y)
        {
            return GetCellOrDefault(x, y) != null;
        }

        public ICell GetCellOrDefault(int x, int y)
        {
            return CurrentGeneration.FirstOrDefault(c => c.X == x && c.Y == y);
        }

        public void Process()
        {
            var nextGeneration = algorithm.DetermineNextGeneration(this);

            CurrentGeneration.Clear();
            CurrentGeneration.UnionWith(nextGeneration);
        }

        public int CountAliveNeighbours(int x, int y)
        {
            var options = new List<(int, int)>
            {
                (x - 1, y - 1),
                (x, y - 1),
                (x + 1, y - 1),
                (x - 1, y),
                (x + 1, y),
                (x - 1, y + 1),
                (x, y + 1),
                (x + 1, y + 1)
            };

            return options.Count(p => CellExists(p.Item1, p.Item2));
        }
    }

    public interface IBoard
    {
        HashSet<ICell> CurrentGeneration { get; }
        int GetMinX();
        int GetMinY();
        int GetMaxX();
        int GetMaxY();
        void AddCell(int x, int y);
        bool CellExists(int x, int y);
        ICell GetCellOrDefault(int x, int y);
        void Process();
        int CountAliveNeighbours(int x, int y);
    }
}
