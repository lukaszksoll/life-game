﻿using System;

namespace LifeGame.Models
{
    public class Cell : ICell, IEquatable<ICell>
    {
        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public bool Equals(ICell other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (obj is ICell cell)
            {
                return Equals(cell);
            }

            throw new InvalidCastException(nameof(obj));
        }

        public override int GetHashCode()
        {
            return $"{X}_{Y}".GetHashCode();
        }
    }

    public interface ICell
    {
        int X { get; }
        int Y { get; }
    }
}
