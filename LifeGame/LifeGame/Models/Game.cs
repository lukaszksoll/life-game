﻿using LifeGame.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LifeGame.Models
{
    public class Game : IGame
    {
        private readonly IBoard board;
        private readonly ILifeGameSimulator simulator;

        public Game(IBoard board, ILifeGameSimulator simulator)
        {
            this.board = board;
            this.simulator = simulator;
        }

        public void Run(IEnumerable<(int, int)> tuples)
        {
            if (tuples == null)
                throw new ArgumentNullException(nameof(tuples));

            if (!tuples.Any())
                throw new ArgumentException(nameof(tuples));

            foreach ((int item1, int item2) in tuples)
            {
                board.AddCell(item1, item2);
            }

            simulator.Simulate(board);
        }
    }

    public interface IGame
    {
        void Run(IEnumerable<(int, int)> tuples);
    }
}
